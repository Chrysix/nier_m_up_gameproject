
//Movement mouse

image_angle = point_direction(x, y, mouse_x, mouse_y) - 90; // -90 = rotation du sprite devant la souris

//Movement

move_speed = 8;

if (keyboard_check(ord("Z"))) y -= move_speed;
if (keyboard_check(ord("S"))) y += move_speed;
if (keyboard_check(ord("D"))) x += move_speed;
if (keyboard_check(ord("Q"))) x -= move_speed;

//Shoot

if (mouse_check_button (mb_left)) && (cooldown < 1)
{
	instance_create_layer(x, y, "layer_bullet", obj_pbullet);
	cooldown = 10; //valeur pour augmenter la vitesse des tirs
}

cooldown = cooldown - 1;


//Invulnerability Frames

if (place_meeting(x, y, obj_ebullet)){
	hit = true;
} else {
	hit = false;
}

if (hit == true && invincible == false){
	invincible = true;
	alarm_set(0, 180);
	//(placer animation ici)
}

///repousse le joueur du projectile
if (spd_bounce >= 0) 
{
	speed = spd_bounce;
	spd_bounce = spd_bounce - 1;
}


//player death

if (hp_player <= 0){
	game_restart()
}
