
//Position relative du score à en haut à droite
var cx = camera_get_view_x(view_camera[0]);//caméra X
var cy = camera_get_view_y(view_camera[0]);//caméra Y
var cw = camera_get_view_width(view_camera[0]);//largeur de caméra
var ch = camera_get_view_height(view_camera[0]);
var life_score = "Life : " + string(obj_player.hp_player);

//Position score
draw_set_font(fnt_score);
draw_set_halign(fa_left);
draw_set_color(c_red);
draw_text(cx + cw/20, cy + ch/6, life_score);